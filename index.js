let leftArrow = document.getElementById('left-arrow');
let rightArrow = document.getElementById('right-arrow');

let slides = document.getElementsByClassName('slide');

let currentMovie = 0;

rightArrow.onclick = function () {
    currentMovie = (currentMovie === slides.length - 1) ? 0 : currentMovie + 1;
    for (let i = 0; i < slides.length; i++) {
        if (i === currentMovie) {
            slides[i].style.display = 'block';
        } else {
            slides[i].style.display = 'none';
        }
    }
}

leftArrow.onclick = function () {
    currentMovie = (currentMovie === 0) ? slides.length - 1 : currentMovie - 1;
    for (let i = 0; i < slides.length; i++) {
        if (i === currentMovie) {
            slides[i].style.display = 'block';
        } else {
            slides[i].style.display = 'none';
        }
    }
}

function one () {
    let info = document.getElementsByClassName('info-one');

    for (let i = 0 ; i < info.length ; i++) {
        info[i].classList.toggle('info');
    }
}

function two () {
    let info = document.getElementsByClassName('info-two');

    for (let i = 0 ; i < info.length ; i++) {
        info[i].classList.toggle('info');
    }
}

function three () {
    let info = document.getElementsByClassName('info-three');

    for (let i = 0 ; i < info.length ; i++) {
        info[i].classList.toggle('info');
    }
}

function time_oneA () {
    let sliderBox = document.getElementsByClassName('slider');
    let boxTwo = document.getElementsByClassName('exclu-box-two');
    let boxThree = document.getElementsByClassName('exclu-box-three');
    let description = document.getElementsByClassName('description-one');
    let bo = document.getElementsByClassName('bo-1');
    let otherTime = document.getElementsByClassName('time-two_one');
    let numberPlaces = document.getElementsByClassName('places');
    let endButton = document.getElementsByClassName('end');
    let excluTitle = document.getElementsByClassName('title-second-box');
    let valider = document.getElementsByClassName('end-txt');

    for (let i = 0 ; i < sliderBox.length ; i++) {
        sliderBox[i].classList.toggle('info');
        boxTwo[i].classList.toggle('info');
        boxThree[i].classList.toggle('info');
        description[i].classList.toggle('info');
        bo[i].classList.toggle('info');
        otherTime[i].classList.toggle('info');
        numberPlaces[i].classList.toggle('info');
        endButton[i].classList.toggle('info');
        excluTitle[i].classList.toggle('info');
        valider[i].classList.add('info');
    }
}

function time_twoA () {
    let sliderBox = document.getElementsByClassName('slider');
    let boxTwo = document.getElementsByClassName('exclu-box-two');
    let boxThree = document.getElementsByClassName('exclu-box-three');
    let description = document.getElementsByClassName('description-one');
    let bo = document.getElementsByClassName('bo-1');
    let otherTime = document.getElementsByClassName('time-one_one');
    let numberPlaces = document.getElementsByClassName('places');
    let endButton = document.getElementsByClassName('end');
    let excluTitle = document.getElementsByClassName('title-second-box');
    let valider = document.getElementsByClassName('end-txt');

    for (let i = 0 ; i < sliderBox.length ; i++) {
        sliderBox[i].classList.toggle('info');
        boxTwo[i].classList.toggle('info');
        boxThree[i].classList.toggle('info');
        description[i].classList.toggle('info');
        bo[i].classList.toggle('info');
        otherTime[i].classList.toggle('info');
        numberPlaces[i].classList.toggle('info');
        endButton[i].classList.toggle('info');
        excluTitle[i].classList.toggle('info');
        valider[i].classList.add('info');
    }
}

function time_oneB () {
    let sliderBox = document.getElementsByClassName('slider');
    let boxOne = document.getElementsByClassName('exclu-box-one');
    let boxThree = document.getElementsByClassName('exclu-box-three');
    let description = document.getElementsByClassName('description-two');
    let bo = document.getElementsByClassName('bo-2');
    let otherTime = document.getElementsByClassName('time-two_two');
    let numberPlaces = document.getElementsByClassName('places');
    let endButton = document.getElementsByClassName('end');
    let excluTitle = document.getElementsByClassName('title-second-box');
    let valider = document.getElementsByClassName('end-txt');

    for (let i = 0 ; i < sliderBox.length ; i++) {
        sliderBox[i].classList.toggle('info');
        boxOne[i].classList.toggle('info');
        boxThree[i].classList.toggle('info');
        description[i].classList.toggle('info');
        bo[i].classList.toggle('info');
        otherTime[i].classList.toggle('info');
        numberPlaces[i].classList.toggle('info');
        endButton[i].classList.toggle('info');
        excluTitle[i].classList.toggle('info');
        valider[i].classList.add('info');
    }
}

function time_twoB () {
    let sliderBox = document.getElementsByClassName('slider');
    let boxOne = document.getElementsByClassName('exclu-box-one');
    let boxThree = document.getElementsByClassName('exclu-box-three');
    let description = document.getElementsByClassName('description-two');
    let bo = document.getElementsByClassName('bo-2');
    let otherTime = document.getElementsByClassName('time-one_two');
    let numberPlaces = document.getElementsByClassName('places');
    let endButton = document.getElementsByClassName('end');
    let excluTitle = document.getElementsByClassName('title-second-box');
    let valider = document.getElementsByClassName('end-txt');

    for (let i = 0 ; i < sliderBox.length ; i++) {
        sliderBox[i].classList.toggle('info');
        boxOne[i].classList.toggle('info');
        boxThree[i].classList.toggle('info');
        description[i].classList.toggle('info');
        bo[i].classList.toggle('info');
        otherTime[i].classList.toggle('info');
        numberPlaces[i].classList.toggle('info');
        endButton[i].classList.toggle('info');
        excluTitle[i].classList.toggle('info');
        valider[i].classList.add('info');
    }
}

function time_oneC () {
    let sliderBox = document.getElementsByClassName('slider');
    let boxOne = document.getElementsByClassName('exclu-box-one');
    let boxTwo = document.getElementsByClassName('exclu-box-two');
    let description = document.getElementsByClassName('description-three');
    let bo = document.getElementsByClassName('bo-3');
    let otherTime = document.getElementsByClassName('time-two_three');
    let numberPlaces = document.getElementsByClassName('places');
    let endButton = document.getElementsByClassName('end');
    let excluTitle = document.getElementsByClassName('title-second-box');
    let valider = document.getElementsByClassName('end-txt');

    for (let i = 0 ; i < sliderBox.length ; i++) {
        sliderBox[i].classList.toggle('info');
        boxOne[i].classList.toggle('info');
        boxTwo[i].classList.toggle('info');
        description[i].classList.toggle('info');
        bo[i].classList.toggle('info');
        otherTime[i].classList.toggle('info');
        numberPlaces[i].classList.toggle('info');
        endButton[i].classList.toggle('info');
        excluTitle[i].classList.toggle('info');
        valider[i].classList.add('info');
    }
}

function time_twoC () {
    let sliderBox = document.getElementsByClassName('slider');
    let boxOne = document.getElementsByClassName('exclu-box-one');
    let boxTwo = document.getElementsByClassName('exclu-box-two');
    let description = document.getElementsByClassName('description-three');
    let bo = document.getElementsByClassName('bo-3');
    let otherTime = document.getElementsByClassName('time-one_three');
    let numberPlaces = document.getElementsByClassName('places');
    let endButton = document.getElementsByClassName('end');
    let excluTitle = document.getElementsByClassName('title-second-box');
    let valider = document.getElementsByClassName('end-txt');

    for (let i = 0 ; i < sliderBox.length ; i++) {
        sliderBox[i].classList.toggle('info');
        boxOne[i].classList.toggle('info');
        boxTwo[i].classList.toggle('info');
        description[i].classList.toggle('info');
        bo[i].classList.toggle('info');
        otherTime[i].classList.toggle('info');
        numberPlaces[i].classList.toggle('info');
        endButton[i].classList.toggle('info');
        excluTitle[i].classList.toggle('info');
        valider[i].classList.add('info');
    }
}

let number = document.getElementById('number');
let result = document.getElementById('result')

function button () {
    let operation = parseInt(number.value) * 7;
    result.value = parseInt(operation);
}

function finish () {
    let endText = document.getElementsByClassName('end-txt');
    let boxPlaces = document.getElementsByClassName('places');
    let reservation = document.getElementsByClassName('end');
    for (let i = 0 ; i < endText.length ; i++) {
        endText[i].classList.toggle('info');
        boxPlaces[i].classList.toggle('info-second');
        reservation[i].classList.toggle('info-second');
    }
}